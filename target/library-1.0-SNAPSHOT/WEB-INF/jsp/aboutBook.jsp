<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理系统-查看员工</title>
    <!-- Fav  Icon Link -->
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
    <!-- themify icons CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
    <!-- Animations CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/animate.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">
    <!-- morris charts -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/charts/css/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/jquery-jvectormap.css">

    <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body>
<!-- Pre Loader -->
<div class="loading">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
    <span class="theme-color theme-default theme-active" data-color="green"></span>
    <span class="theme-color theme-blue" data-color="blue"></span>
    <span class="theme-color theme-red" data-color="red"></span>
    <span class="theme-color theme-violet" data-color="violet"></span>
    <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
    <!-- Page Content -->
    <div id="content">
        <jsp:include page="header.jsp" />
        <!-- Breadcrumb -->
        <!-- Page Title -->
        <div class="container mt-0">
            <div class="row breadcrumb-bar">
                <div class="col-md-6">
                    <h3 class="block-title">图书详情</h3>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <span class="ti-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item">图书管理</li>
                        <li class="breadcrumb-item active">图书详情</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /Page Title -->

        <!-- /Breadcrumb -->
        <!-- Main Content -->
        <div class="container">

            <div class="row">
                <!-- Widget Item -->
                <div class="col-md-12">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title">图书详情</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td><strong>书名</strong></td>
                                    <td>${requestScope.book.title}</td>
                                </tr>
                                <tr>
                                    <td><strong>作者</strong> </td>
                                    <td>${requestScope.book.author}</td>
                                </tr>
                                <tr>
                                    <td><strong>出版商</strong></td>
                                    <td>${requestScope.book.publisher}</td>
                                </tr>
                                <tr>
                                    <td><strong>出版时间</strong></td>
                                    <td><fmt:formatDate value="${requestScope.book.publishTime}" pattern="yyyy-MM-dd"/> </td>
                                </tr>
                                <tr>
                                    <td><strong>价格</strong></td>
                                    <td>${requestScope.book.price}</td>
                                </tr>

                                <tr>
                                    <td><strong>库存</strong></td>
                                    <td>${requestScope.book.stock}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
            </div>
        </div>
        <!-- /Main Content -->
        <!--Copy Rights-->
        <jsp:include page="footer.jsp" />
        <!-- /Copy Rights-->
    </div>
    <!-- /Page Content -->
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
    <span class="ti-angle-up"></span>
</a>
<!-- /Back to Top -->
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- morris charts -->
<script src="${pageContext.request.contextPath}/statics/charts/js/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/statics/charts/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/statics/js/custom-morris.js"></script>

<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>
</body>

</html>