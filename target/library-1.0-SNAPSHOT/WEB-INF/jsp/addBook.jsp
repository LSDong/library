<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>员工管理系统-添加员工</title>
  <!-- Fav  Icon Link -->
  <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
  <!-- themify icons CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
  <!-- Animations CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/animate.css">
  <!-- Main CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">
  <!-- morris charts -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/charts/css/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/jquery-jvectormap.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/datatable/dataTables.bootstrap4.min.css">

  <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body>
<!-- Pre Loader -->
<div class="loading">
  <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
  <span class="theme-color theme-default theme-active" data-color="green"></span>
  <span class="theme-color theme-blue" data-color="blue"></span>
  <span class="theme-color theme-red" data-color="red"></span>
  <span class="theme-color theme-violet" data-color="violet"></span>
  <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
  <!-- Page Content -->
  <div id="content">
    <%-- 导入文件头部 --%>
    <jsp:include page="header.jsp" />
    <!-- Breadcrumb -->
    <!-- Page Title -->
    <div class="container mt-0">
      <div class="row breadcrumb-bar">
        <div class="col-md-6">
          <h3 class="block-title">添加图书信息</h3>
        </div>
        <div class="col-md-6">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">
                <span class="ti-home"></span>
              </a>
            </li>
            <li class="breadcrumb-item">图书管理</li>
            <li class="breadcrumb-item active">添加图书信息</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- /Page Title -->

    <!-- /Breadcrumb -->
    <!-- Main Content -->
    <div class="container">

      <div class="row">
        <!-- Widget Item -->
        <div class="col-md-12">
          <div class="widget-area-2 lochana-box-shadow">
            <h3 class="widget-title">添加图书信息</h3>
            <form action="${pageContext.request.contextPath}/book/add" method="post">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="patient-name">书名</label>
                  <input name="title" type="text" class="form-control" placeholder="请输入书名" id="patient-name">
                </div>

                <div class="form-group col-md-6">
                  <label for="author">作者</label>
                  <input name="author" type="text" class="form-control" placeholder="请输入作者" id="author">
                </div>

                <div class="form-group col-md-6">
                  <label for="publisher">出版商</label>
                  <input name="publisher" type="text" class="form-control" placeholder="请输入出版商" id="publisher">
                </div>

                <div class="form-group col-md-6">
                  <label for="publishTime">出版时间</label>
                  <input name="publishTime" type="Date" class="form-control" placeholder="请选择出版时间" id="publishTime">
                </div>

                <div class="form-group col-md-6">
                  <label for="price">价格</label>
                  <input name="price" type="text" class="form-control" placeholder="请输入价格" id="price">
                </div>

                <div class="form-group col-md-6">
                  <label for="stock">库存</label>
                  <input name="stock" type="text" placeholder="请输入库存" class="form-control" id="stock">
                </div>



                <div class="form-check col-md-12 mb-2">
                  <div class="text-left">
                    <div class="custom-control custom-checkbox">
                      <input class="custom-control-input" type="checkbox" id="ex-check-2">
                      <label class="custom-control-label" for="ex-check-2">Please Confirm</label>
                    </div>
                  </div>
                </div>
                <div class="form-group col-md-6 mb-3">
                  <input type="submit" class="btn btn-primary btn-lg" value="新增">
                </div>
              </div>
            </form>

          </div>
        </div>
        <!-- /Widget Item -->
      </div>
    </div>
    <!-- /Main Content -->
    <%-- 页面底部的版权声明 --%>
    <jsp:include page="footer.jsp" />
  </div>
  <!-- /Page Content -->
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
  <span class="ti-angle-up"></span>
</a>
<!-- /Back to Top -->
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>

<!-- Datatable  -->
<script src="${pageContext.request.contextPath}/statics/datatable/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/statics/datatable/dataTables.bootstrap4.min.js"></script>

<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>

</body>

</html>