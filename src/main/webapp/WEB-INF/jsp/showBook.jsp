<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理系统-员工列表</title>
    <!-- Fav  Icon Link -->
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
    <!-- themify icons CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
    <!-- Animations CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/animate.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">
    <!-- morris charts -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/charts/css/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/jquery-jvectormap.css">

    <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body>
<!-- Pre Loader -->
<div class="loading">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
    <span class="theme-color theme-default theme-active" data-color="green"></span>
    <span class="theme-color theme-blue" data-color="blue"></span>
    <span class="theme-color theme-red" data-color="red"></span>
    <span class="theme-color theme-violet" data-color="violet"></span>
    <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
    <!-- Page Content -->
    <div id="content">
        <jsp:include page="header.jsp"/>
        <!-- Breadcrumb -->
        <!-- Page Title -->
        <div class="container mt-0">
            <div class="row breadcrumb-bar">
                <div class="col-md-6">
                    <h3 class="block-title">员工管理</h3>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <span class="ti-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item">员工管理</li>
                        <li class="breadcrumb-item active">员工列表</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /Page Title -->

        <!-- /Breadcrumb -->
        <!-- Main Content -->
        <div class="container">

            <div class="row">
                <!-- Widget Item -->
                <div class="col-md-12">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title">图书列表</h3>

                        <form action="${pageContext.request.contextPath}/book/page">
                            <input type="text" name="title" placeholder="请输入书名进行搜索" value="${title}">
                            <input type="submit" value="搜索">
                        </form>
                        <div class="table-responsive mb-3">
                            <table id="tableId" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>图书ID</th>
                                    <th>书名</th>
                                    <th>作者</th>
                                    <th>出版商</th>
                                    <th>出版时间</th>
                                    <th>价格</th>
                                    <th>库存</th>
                                </tr>
                                </thead>
                                <tbody>

                                <c:forEach items="${requestScope.pageInfo.list}" var="book">
                                    <tr>
                                        <td>${book.id}</td>
                                        <td>${book.title}</td>
                                        <td>${book.author}</td>
                                        <td>${book.publisher}</td>
                                        <td><fmt:formatDate value="${book.publishTime}" pattern="yyyy-MM-dd"/></td>
                                        <td>${book.price}</td>
                                        <td>${book.stock}</td>
                                        <td>
                                            <span class="badge badge-danger"><a
                                                    href="${pageContext.request.contextPath}/book/delete?id=${book.id}">删除</a></span>
                                            <span class="badge badge-success"><a
                                                    href="${pageContext.request.contextPath}/book/toEdit?id=${book.id}">修改</a></span>
                                            <span class="badge badge-primary"><a
                                                    href="${pageContext.request.contextPath}/book/one?id=${book.id}">查看</a></span>
                                        </td>
                                    </tr>
                                </c:forEach>

                                </tbody>
                            </table>
                            <!--Export links-->
                            <div class="lochana-widget">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">

                                        <%-- 上一页 --%>
                                        <c:if test="${requestScope.pageInfo.hasPreviousPage}">
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="${pageContext.request.contextPath}/book/page?page=${requestScope.pageInfo.prePage}&title=${title}"
                                                   aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                        </c:if>


                                        <c:forEach begin="1" end="${requestScope.pageInfo.pages}" var="p">
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="${pageContext.request.contextPath}/book/page?page=${p}&title=${title}">${p}</a>
                                            </li>
                                        </c:forEach>

                                        <%-- 下一页 --%>
                                        <c:if test="${requestScope.pageInfo.hasNextPage}">
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="${pageContext.request.contextPath}/book/page?page=${requestScope.pageInfo.nextPage}&title=${title}"
                                                   aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </c:if>
                                    </ul>
                                </nav>
                            </div>
                            <!-- /Export links-->
<%--                            <button type="button" class="btn btn-danger mt-3 mb-0"><span class="ti-trash"--%>
<%--                            ></span>--%>
<%--                                DELETE--%>
<%--                            </button>--%>
<%--                            <button type="button" class="btn btn-primary mt-3 mb-0"><span--%>
<%--                                    class="ti-pencil-alt"></span> EDIT--%>
<%--                            </button>--%>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
            </div>
        </div>
        <!-- /Main Content -->
        <jsp:include page="footer.jsp"/>
    </div>
    <!-- /Page Content -->
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
    <span class="ti-angle-up"></span>
</a>
<!-- /Back to Top -->
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- morris charts -->
<script src="${pageContext.request.contextPath}/statics/charts/js/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/statics/charts/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/statics/js/custom-morris.js"></script>

<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>

</body>

</html>
