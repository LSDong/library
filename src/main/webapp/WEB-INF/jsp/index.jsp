<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理系统</title>
    <!-- Fav  Icon Link -->
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
    <!-- themify icons CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
    <!-- Animations CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/animate.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">
    <!-- morris charts -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/charts/css/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/jquery-jvectormap.css">

    <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body>
<!-- Pre Loader -->
<div class="loading">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
    <span class="theme-color theme-default theme-active" data-color="green"></span>
    <span class="theme-color theme-blue" data-color="blue"></span>
    <span class="theme-color theme-red" data-color="red"></span>
    <span class="theme-color theme-violet" data-color="violet"></span>
    <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
    <!-- Page Content -->
    <div id="content">
        <%-- 导入文件头部 --%>
       <jsp:include page="header.jsp" />
        <!-- Breadcrumb -->
        <!-- Page Title -->
        <div class="container mt-0">
            <div class="row breadcrumb-bar">
                <div class="col-md-6">
                    <h3 class="block-title">快速统计</h3>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <span class="ti-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">控制面板</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /Page Title -->
        <!-- /Breadcrumb -->
        <!-- Main Content -->
        <div class="container home">
            <div class="row">
                <!-- Widget Item -->
                <div class="col-md-4">
                    <div class="widget-area lochana-box-shadow color-red">
                        <div class="widget-left">
                            <span class="ti-user"></span>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">病人</h4>
                            <span class="numeric color-red">348</span>
                            <p class="inc-dec mb-0"><span class="ti-angle-up"></span> +20% 增长</p>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
                <!-- Widget Item -->
                <div class="col-md-4">
                    <div class="widget-area lochana-box-shadow color-green">
                        <div class="widget-left">
                            <span class="ti-bar-chart"></span>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">出诊</h4>
                            <span class="numeric color-green">1585</span>
                            <p class="inc-dec mb-0"><span class="ti-angle-down"></span> -15% 减少</p>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
                <!-- Widget Item -->
                <div class="col-md-4">
                    <div class="widget-area lochana-box-shadow color-yellow">
                        <div class="widget-left">
                            <span class="ti-money"></span>
                        </div>
                        <div class="widget-right">
                            <h4 class="wiget-title">收入</h4>
                            <span class="numeric color-yellow">$7300</span>
                            <p class="inc-dec mb-0"><span class="ti-angle-up"></span> +10% 增长</p>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
            </div>

            <div class="row">
                <!-- Widget Item -->
                <div class="col-md-6">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title">出诊按年统计</h3>
                        <div id="lineMorris" class="chart-home"></div>
                    </div>
                </div>
                <!-- /Widget Item -->
                <!-- Widget Item -->
                <div class="col-md-6">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title"> 病人按年统计</h3>
                        <div id="barMorris" class="chart-home"></div>
                    </div>
                </div>
                <!-- /Widget Item -->
            </div>

            <div class="row">
                <!-- Widget Item -->
                <div class="col-md-12">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title">出诊</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>名人姓名</th>
                                    <th>医生</th>
                                    <th>检查</th>
                                    <th>日期</th>
                                    <th>时间</th>
                                    <th>状态</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Rajesh</td>
                                    <td>Manoj Kumar</td>
                                    <td>牙科</td>
                                    <td>12-10-2018</td>
                                    <td>12:10PM</td>
                                    <td>
                                        <span class="badge badge-success">Completed</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Riya</td>
                                    <td>Daniel </td>
                                    <td>皮肤科</td>
                                    <td>12-10-2018</td>
                                    <td>1:10PM</td>
                                    <td>
                                        <span class="badge badge-warning">Pending</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Siri</td>
                                    <td>Daniel </td>
                                    <td>皮肤科</td>
                                    <td>12-10-2018</td>
                                    <td>1:30PM</td>
                                    <td>
                                        <span class="badge badge-danger">Cancelled</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rajesh</td>
                                    <td>Manoj Kumar</td>
                                    <td>牙科</td>
                                    <td>12-10-2018</td>
                                    <td>12:10PM</td>
                                    <td>
                                        <span class="badge badge-success">Completed</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Riya</td>
                                    <td>Daniel </td>
                                    <td>皮肤科</td>
                                    <td>12-10-2018</td>
                                    <td>1:10PM</td>
                                    <td>
                                        <span class="badge badge-warning">Pending</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Siri</td>
                                    <td>Daniel </td>
                                    <td>皮肤科</td>
                                    <td>12-10-2018</td>
                                    <td>1:30PM</td>
                                    <td>
                                        <span class="badge badge-danger">Cancelled</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Widget Item -->
            </div>

            <div class="row">
                <!-- Widget Item -->
                <div class="col-sm-6">
                    <div class="widget-area-2 lochana-box-shadow">
                        <h3 class="widget-title">出诊 状态</h3>
                        <div id="donutMorris" class="chart-home"></div>
                    </div>
                </div>
                <!-- /Widget Item -->
                <!-- Widget Item -->
                <div class="col-md-6">
                    <div class="widget-area-2 progress-status lochana-box-shadow">
                        <h3 class="widget-title">医生情况</h3>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>医生</th>
                                    <th>科室</th>
                                    <th>情况</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Rajesh</td>
                                    <td>牙科</td>
                                    <td>
                                        <span class="badge badge-success">可出诊</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Riya</td>
                                    <td>皮肤科</td>
                                    <td>
                                        <span class="badge badge-warning">休假</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Siri</td>
                                    <td>眼科</td>
                                    <td>
                                        <span class="badge badge-danger">不可出诊</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rajesh</td>
                                    <td>牙科</td>
                                    <td>
                                        <span class="badge badge-success">可出诊</span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- /Widget Item -->

            </div>

        </div>
        <!-- /Main Content -->
        <%-- 导入页面底部 --%>
        <jsp:include page="footer.jsp" />
    </div>
    <!-- /Page Content -->
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
    <span class="ti-angle-up"></span>
</a>
<!-- /Back to Top -->
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- morris charts -->
<script src="${pageContext.request.contextPath}/statics/charts/js/raphael-min.js"></script>
<script src="${pageContext.request.contextPath}/statics/charts/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/statics/js/custom-morris.js"></script>

<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>
</body>

</html>