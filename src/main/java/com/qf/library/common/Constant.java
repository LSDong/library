package com.qf.library.common;

/**
 * 字典常量类
 *
 * @author zed
 * @date 2023/05/16
 */
public interface Constant {

    // session中的用户key
    String LOGIN_USER = "loginUser";

    // session中的验证码key
    String CAPTCHA = "captcha";
}
