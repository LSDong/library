package com.qf.library.exception;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomExceptionResolver implements HandlerExceptionResolver {

    // 处理异常的方法
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {
        CustomException exception;
        if(e instanceof CustomException){
            exception = (CustomException)e;
        }else {
            exception = new CustomException(e.getMessage());
        }
        ModelAndView mv = new ModelAndView();
        mv.setViewName("error");
        mv.addObject("error",exception.getMessage());
        return mv;
    }
}
