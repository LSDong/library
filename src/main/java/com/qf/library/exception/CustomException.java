package com.qf.library.exception;

import java.io.Serializable;

public class CustomException extends Exception implements Serializable {
    private static final long serialVersionUID = 8520717012873362599L;

    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }
}
