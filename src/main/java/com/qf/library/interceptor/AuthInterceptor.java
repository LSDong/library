package com.qf.library.interceptor;

import com.qf.library.common.Constant;
import com.qf.library.entity.Manager;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * 身份验证拦截器
 *
 * @author zed
 * @date 2023/05/16
 */
public class AuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Manager manager = (Manager) session.getAttribute(Constant.LOGIN_USER);
        if (Objects.isNull(manager)) {
            // 没有登录过 就拦截 并跳转到错误页面显示没有权限的提示
            request.getRequestDispatcher("/error.jsp").forward(request, response);
            return false;
        } else {
            // 如果用户登录过 就放行
            return true;
        }
    }
}
