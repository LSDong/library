package com.qf.library.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    private Integer id;

    private String title;

    private String author;

    private String publisher;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishTime;

    private Double price;

    private Integer stock;

}
