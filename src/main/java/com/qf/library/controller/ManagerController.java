package com.qf.library.controller;

import com.qf.library.common.Constant;
import com.qf.library.entity.Manager;
import com.qf.library.exception.CustomException;
import com.qf.library.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("mng")
@RequiredArgsConstructor
public class ManagerController {

    final ManagerService managerService;

    @RequestMapping("login")
    public String login(String name, String password,String code, HttpSession session) throws CustomException {
        String captcha = (String) session.getAttribute(Constant.CAPTCHA);
        if (Objects.equals(captcha, code)) {
            Manager manager = managerService.login(name, password);
            if(Objects.nonNull(manager)){
                session.setAttribute(Constant.LOGIN_USER,manager);
                return "/WEB-INF/jsp/index";
            }else {
                throw new CustomException("用户名或密码错误");
            }
        }else {
            throw new CustomException("验证码错误");
        }
    }

    @RequestMapping("select")
    public String selectAll(HttpServletRequest request){
        List<Manager> managers = managerService.selectAll();
        request.setAttribute("managers",managers);
        return"/WEB-INF/jsp/showManager";
    }

    @PostMapping("register")
    public String register(Manager manager) {
        // 参数校验
        Assert.hasText(manager.getName(), "用户名不能为空");
        Assert.hasText(manager.getPassword(), "密码不能为空");
        // 用户名不能是已经存在的
        Assert.isTrue(managerService.selectByName(manager.getName()), "用户已经存在");
        // 校验都通过后开始注册
        managerService.register(manager);
        return "redirect:/login.jsp";
    }




}
