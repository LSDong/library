package com.qf.library.controller;


import com.github.pagehelper.PageInfo;
import com.qf.library.entity.Book;
import com.qf.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequiredArgsConstructor
@RequestMapping("book")
public class BookController {

    final BookService bookService;

    @RequestMapping("page")
    public String page(@RequestParam(value = "page",required = false,defaultValue = "1") Integer page,
                       @RequestParam(value = "size",required = false,defaultValue = "3") Integer size,
                       String title, HttpServletRequest request){
        PageInfo<Book> pageInfo = bookService.selectByPage(page, size, title);
        request.setAttribute("pageInfo",pageInfo);
        request.setAttribute("title",title);
        return "/WEB-INF/jsp/showBook";
    }

    @GetMapping("one")
    public String selectOne(Integer id,HttpServletRequest request){
        Book book = bookService.selectOne(id);
        request.setAttribute("book",book);
        return "/WEB-INF/jsp/aboutBook";
    }
    @RequestMapping("toAdd")
    public String toAdd(){
        return "/WEB-INF/jsp/addBook";
    }

    @PostMapping("add")
    public String add(Book book){
        bookService.insert(book);
        return "redirect:/book/page";
    }

    @GetMapping("toEdit")
    public String toEdit(Integer id,HttpServletRequest request){
        Book book = bookService.selectOne(id);
        request.setAttribute("book",book);
        return "/WEB-INF/jsp/updateBook";
    }

    @PostMapping("update")
    public String update(Book book){
        bookService.update(book);
        return "redirect:/book/page";
    }

    @GetMapping("delete")
    public String delete(Integer id){
        bookService.deleteById(id);

        return "redirect:/book/page";
    }

//    @RequestMapping("upsk")
//    public String updateStock(Book book){
//         bookService.updateStock(book);
//    }
}
