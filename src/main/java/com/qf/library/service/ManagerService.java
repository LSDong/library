package com.qf.library.service;

import com.qf.library.entity.Manager;

import java.util.List;

public interface ManagerService {
    Manager login(String name,String password);

    List<Manager> selectAll();


    Boolean register(Manager manager);

    boolean selectByName(String name);
}
