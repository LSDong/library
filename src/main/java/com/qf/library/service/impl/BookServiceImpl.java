package com.qf.library.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.library.dao.BookMapper;
import com.qf.library.entity.Book;
import com.qf.library.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class BookServiceImpl implements BookService {

    final BookMapper bookMapper;
    @Override
    public PageInfo<Book> selectByPage(Integer page, Integer size, String title) {
        PageHelper.startPage(page,size);
        List<Book> books = bookMapper.selectAll(title);
        PageInfo<Book> info = new PageInfo<>(books);
        return info;
    }

    @Override
    public Book selectOne(Integer id) {
        return bookMapper.selectById(id);
    }

    @Override
    public Boolean insert(Book book) {
        return bookMapper.insert(book)>0;
    }

    @Override
    public Boolean update(Book book) {
        return bookMapper.update(book)>0;
    }

    @Override
    public Boolean deleteById(Integer id) {
        return bookMapper.deleteById(id)>0;
    }

    @Override
    public Boolean updateStock(Book book) {

        return bookMapper.updateStock(book)>0;
    }


}
