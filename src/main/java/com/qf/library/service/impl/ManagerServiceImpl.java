package com.qf.library.service.impl;

import com.qf.library.dao.ManagerMapper;
import com.qf.library.entity.Manager;
import com.qf.library.service.ManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ManagerServiceImpl implements ManagerService {

    final ManagerMapper managerMapper;
    @Override
    public Manager login(String name, String password) {
        log.info("{}开始登录啦"+name);
        List<Manager> managers = managerMapper.selectByName(name);
        for (Manager manager : managers) {
            if (manager.getPassword().equals(password)){
                return manager;
            }
        }

        return null;
    }

    @Override
    public List<Manager> selectAll() {
        return managerMapper.selectAll();
    }

    @Override
    public Boolean register(Manager manager) {
        return managerMapper.insert(manager)>0;
    }

    @Override
    public boolean selectByName(String name) {
        List<Manager> managers = managerMapper.selectByName(name);
        return CollectionUtils.isEmpty(managers);
    }
}
