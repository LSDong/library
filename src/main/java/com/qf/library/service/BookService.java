package com.qf.library.service;

import com.github.pagehelper.PageInfo;
import com.qf.library.entity.Book;

public interface BookService {
    PageInfo<Book> selectByPage(Integer page,Integer size,String title);

    Book selectOne(Integer id);

    Boolean insert(Book book);

    Boolean update(Book book);

    Boolean deleteById(Integer id);

    Boolean updateStock(Book book);
}
