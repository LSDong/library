package com.qf.library.dao;

import com.qf.library.entity.Manager;

import java.util.List;

public interface ManagerMapper {

    List<Manager> selectByName(String name);

    List<Manager> selectAll();

    int insert(Manager manager);
 }
