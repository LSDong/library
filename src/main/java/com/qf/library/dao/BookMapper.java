package com.qf.library.dao;

import com.qf.library.entity.Book;

import java.util.List;

public interface BookMapper {
    List<Book> selectAll(String title);

    int deleteById(Integer id);

    int update(Book book);

    int insert(Book book);

    Book selectById(Integer id);

    int updateStock(Book book);
}
